include(defs)nat64`'dnl
sys_ni_syscall
sys_exit
sys_read
sys_write
open
close
sys_creat
link
unlink
ia64_execve
chdir
sys_fchdir
utimes
mknod
chmod
sys_chown
sys_lseek
getpid
sys_getppid
mount
umount
sys_setuid
sys_getuid
sys_geteuid
sys_ptrace
access
sys_sync
sys_fsync
sys_fdatasync
sys_kill
rename
mkdir
rmdir
sys_dup
sys_ia64_pipe
times
ia64_brk
sys_setgid
sys_getgid
sys_getegid
sys_acct
sys_ioctl
sys_fcntl
sys_umask
sys_chroot
sys_ustat
sys_dup2
sys_setreuid
sys_setregid
sys_getresuid
sys_setresuid
sys_getresgid
sys_setresgid
sys_getgroups
sys_setgroups
sys_getpgid
sys_setpgid
sys_setsid
sys_getsid
sys_sethostname
sys_setrlimit
sys_getrlimit
sys_getrusage
sys_gettimeofday
sys_settimeofday
sys_select
sys_poll
symlink
readlink
sys_uselib
sys_swapon
sys_swapoff
sys_reboot
sys_truncate
sys_ftruncate
fchmod
sys_fchown
ia64_getpriority
sys_setpriority
sys_statfs
sys_fstatfs
sys_gettid
sys_semget
sys_semop
sys_semctl
sys_msgget
sys_msgsnd
sys_msgrcv
sys_msgctl
sys_shmget
sys_shmat
sys_shmdt
sys_shmctl
sys_syslog
setitimer
getitimer
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
sys_vhangup
sys_lchown
sys_remap_file_pages
sys_wait4
sys_sysinfo
sys_clone
sys_setdomainname
sys_newuname
sys_adjtimex
sys_ni_syscall
sys_init_module
sys_delete_module
sys_ni_syscall
sys_ni_syscall
sys_quotactl
sys_bdflush
sys_sysfs
sys_personality
sys_ni_syscall
sys_setfsuid
sys_setfsgid
sys_getdents
sys_flock
sys_readv
sys_writev
sys_pread64
sys_pwrite64
sys_sysctl
sys_mmap
sys_munmap
sys_mlock
sys_mlockall
sys_mprotect
ia64_mremap
sys_msync
sys_munlock
sys_munlockall
sched_getparam
sched_setparam
sched_getscheduler
sched_setscheduler
sched_yield
sched_get_priority_max
sched_get_priority_min
sched_rr_get_interval
nanosleep
sys_ni_syscall
sys_prctl
sys_getpagesize
sys_mmap2
sys_pciconfig_read
sys_pciconfig_write
sys_perfmonctl
sys_sigaltstack
sys_rt_sigaction
sys_rt_sigpending
sys_rt_sigprocmask
sys_rt_sigqueueinfo
sys_rt_sigreturn
sys_rt_sigsuspend
sys_rt_sigtimedwait
sys_getcwd
sys_capget
sys_capset
sys_sendfile64
sys_ni_syscall
sys_ni_syscall
sys_socket
sys_bind
sys_connect
sys_listen
sys_accept
sys_getsockname
sys_getpeername
sys_socketpair
sys_send
sys_sendto
sys_recv
sys_recvfrom
sys_shutdown
sys_setsockopt
sys_getsockopt
sys_sendmsg
sys_recvmsg
sys_pivot_root
sys_mincore
sys_madvise
sys_newstat
sys_newlstat
sys_newfstat
sys_clone2
sys_getdents64
sys_getunwind
sys_readahead
setxattr
lsetxattr
fsetxattr
getxattr
lgetxattr
fgetxattr
listxattr
llistxattr
flistxattr
removexattr
lremovexattr
fremovexattr
sys_tkill
sys_futex
sched_setaffinity
sched_getaffinity
sys_set_tid_address
sys_fadvise64_64
sys_tgkill
sys_exit_group
sys_lookup_dcookie
io_setup
io_destroy
io_getevents
io_submit
io_cancel
sys_epoll_create
sys_epoll_ctl
sys_epoll_wait
sys_restart_syscall
sys_semtimedop
timer_create
timer_settime
timer_gettime
timer_getoverrun
timer_delete
clock_settime
clock_gettime
clock_getres
clock_nanosleep
sys_fstatfs64
sys_statfs64
sys_mbind
sys_get_mempolicy
sys_set_mempolicy
sys_mq_open
sys_mq_unlink
sys_mq_timedsend
sys_mq_timedreceive
sys_mq_notify
sys_mq_getsetattr
sys_kexec_load
sys_ni_syscall
sys_waitid
sys_add_key
sys_request_key
sys_keyctl
sys_ioprio_set
sys_ioprio_get
sys_move_pages
sys_inotify_init
sys_inotify_add_watch
sys_inotify_rm_watch
sys_migrate_pages
openat
mkdirat
mknodat
sys_fchownat
futimesat
sys_newfstatat
unlinkat
renameat
linkat
symlinkat
readlinkat
fchmodat
faccessat
sys_pselect6
sys_ppoll
sys_unshare
sys_splice
sys_set_robust_list
sys_get_robust_list
sys_sync_file_range
sys_tee
sys_vmsplice
sys_fallocate
sys_getcpu
sys_epoll_pwait
utimensat
signalfd
sys_ni_syscall
sys_eventfd
sys_timerfd_create
sys_timerfd_settime
sys_timerfd_gettime
signalfd4
sys_eventfd2
sys_epoll_create1
sys_dup3
sys_pipe2
sys_inotify_init1
sys_preadv
sys_pwritev
sys_rt_tgsigqueueinfo
sys_recvmmsg
sys_fanotify_init
sys_fanotify_mark
sys_prlimit64
sys_name_to_handle_at
open_by_handle_at
clock_adjtime
sys_syncfs
sys_setns
sys_sendmmsg
sys_process_vm_readv
sys_process_vm_writev
sys_accept4
