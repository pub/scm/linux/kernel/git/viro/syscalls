all: $(patsubst %.m4,res/%,$(wildcard *.m4)) 
	@diff -urN res tables

clean:
	@rm -f res/*

res:
	@mkdir res

res/%: %.m4 defs calls res
	@m4 $< >$@
