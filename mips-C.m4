include(defs)c32`'dnl
sys32_syscall
sys_exit
sys_fork
sys_read
sys_write
open
close
sys_waitpid
sys_creat
link
unlink
execve
chdir
time
mknod
chmod
sys_lchown
sys_ni_syscall
sys_ni_syscall
sys_lseek
getpid
mount
oldumount
sys_setuid
sys_getuid
stime
compat_sys_ptrace
sys_alarm
sys_ni_syscall
sys_pause
utime
sys_ni_syscall
sys_ni_syscall
access
nice
sys_ni_syscall
sys_sync
sys_kill
rename
mkdir
rmdir
sys_dup
sysm_pipe
times
sys_ni_syscall
sys_brk
sys_setgid
sys_getgid
sys_ni_syscall
sys_geteuid
sys_getegid
sys_acct
umount
sys_ni_syscall
compat_sys_ioctl
compat_sys_fcntl
sys_ni_syscall
sys_setpgid
sys_ni_syscall
sys_olduname
sys_umask
sys_chroot
compat_sys_ustat
sys_dup2
sys_getppid
sys_getpgrp
sys_setsid
sys_32_sigaction
sys_sgetmask
sys_ssetmask
sys_setreuid
sys_setregid
sys32_sigsuspend
compat_sys_sigpending
sys_sethostname
compat_sys_setrlimit
compat_sys_getrlimit
compat_sys_getrusage
compat_sys_gettimeofday
compat_sys_settimeofday
sys_getgroups
sys_setgroups
sys_ni_syscall
symlink
sys_ni_syscall
readlink
sys_uselib
sys_swapon
sys_reboot
compat_sys_old_readdir
sys_mips_mmap
sys_munmap
sys_truncate
sys_ftruncate
fchmod
sys_fchown
sys_getpriority
sys_setpriority
sys_ni_syscall
compat_sys_statfs
compat_sys_fstatfs
sys_ni_syscall
compat_sys_socketcall
sys_syslog
setitimer
getitimer
compat_sys_newstat
compat_sys_newlstat
compat_sys_newfstat
sys_uname
sys_ni_syscall
sys_vhangup
sys_ni_syscall
sys_ni_syscall
compat_sys_wait4
sys_swapoff
compat_sys_sysinfo
sys_32_ipc
sys_fsync
sys32_sigreturn
sys32_clone
sys_setdomainname
sys_newuname
sys_ni_syscall
compat_sys_adjtimex
sys_mprotect
compat_sys_sigprocmask
sys_ni_syscall
sys_init_module
sys_delete_module
sys_ni_syscall
sys_quotactl
sys_getpgid
sys_fchdir
sys_bdflush
sys_sysfs
sys_32_personality
sys_ni_syscall
sys_setfsuid
sys_setfsgid
sys_32_llseek
compat_sys_getdents
compat_sys_select
sys_flock
sys_msync
compat_sys_readv
compat_sys_writev
sys_cacheflush
sys_cachectl
sys_sysmips
sys_ni_syscall
sys_getsid
sys_fdatasync
compat_sys_sysctl
sys_mlock
sys_munlock
sys_mlockall
sys_munlockall
sched_setparam
sched_getparam
sched_setscheduler
sched_getscheduler
sched_yield
sched_get_priority_max
sched_get_priority_min
sched_rr_get_interval
nanosleep
sys_mremap
sys_accept
sys_bind
sys_connect
sys_getpeername
sys_getsockname
sys_getsockopt
sys_listen
compat_sys_recv
compat_sys_recvfrom
compat_sys_recvmsg
sys_send
compat_sys_sendmsg
sys_sendto
compat_sys_setsockopt
sys_shutdown
sys_socket
sys_socketpair
sys_setresuid
sys_getresuid
sys_ni_syscall
sys_poll
sys_ni_syscall
sys_setresgid
sys_getresgid
sys_prctl
sys32_rt_sigreturn
sys_32_rt_sigaction
compat_sys_rt_sigprocmask
compat_sys_rt_sigpending
compat_sys_rt_sigtimedwait
compat_sys_rt_sigqueueinfo
compat_sys_rt_sigsuspend
sys_32_pread
sys_32_pwrite
sys_chown
sys_getcwd
sys_capget
sys_capset
compat_sys_sigaltstack
sys_32_sendfile
sys_ni_syscall
sys_ni_syscall
sys_mips_mmap2
sys_32_truncate64
sys_32_ftruncate64
sys_newstat
sys_newlstat
sys_newfstat
sys_pivot_root
sys_mincore
sys_madvise
sys_getdents64
compat_sys_fcntl64
sys_ni_syscall
sys_gettid
sys32_readahead
setxattr
lsetxattr
fsetxattr
getxattr
lgetxattr
fgetxattr
listxattr
llistxattr
flistxattr
removexattr
lremovexattr
fremovexattr
sys_tkill
sys_sendfile64
sys_32_futex
sched_setaffinity
sched_getaffinity
io_setup
io_destroy
io_getevents
io_submit
io_cancel
sys_exit_group
sys32_lookup_dcookie
sys_epoll_create
sys_epoll_ctl
sys_epoll_wait
sys_remap_file_pages
sys_set_tid_address
sys_restart_syscall
sys32_fadvise64_64
compat_sys_statfs64
compat_sys_fstatfs64
timer_create
timer_settime
timer_gettime
timer_getoverrun
timer_delete
clock_settime
clock_gettime
clock_getres
clock_nanosleep
sys_tgkill
utimes
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
compat_sys_mq_open
sys_mq_unlink
compat_sys_mq_timedsend
compat_sys_mq_timedreceive
compat_sys_mq_notify
compat_sys_mq_getsetattr
sys_ni_syscall
sys_32_waitid
sys_ni_syscall
sys_add_key
sys_request_key
sys_keyctl
sys_set_thread_area
sys_inotify_init
sys_inotify_add_watch
sys_inotify_rm_watch
sys_migrate_pages
openat
mkdirat
mknodat
sys_fchownat
futimesat
sys_newfstatat
unlinkat
renameat
linkat
symlinkat
readlinkat
fchmodat
faccessat
compat_sys_pselect6
compat_sys_ppoll
sys_unshare
sys_splice
sys32_sync_file_range
sys_tee
compat_sys_vmsplice
compat_sys_move_pages
compat_sys_set_robust_list
compat_sys_get_robust_list
compat_sys_kexec_load
sys_getcpu
compat_sys_epoll_pwait
sys_ioprio_set
sys_ioprio_get
utimensat
signalfd
sys_ni_syscall
sys_eventfd
sys32_fallocate
sys_timerfd_create
compat_sys_timerfd_gettime
compat_sys_timerfd_settime
signalfd4
sys_eventfd2
sys_epoll_create1
sys_dup3
sys_pipe2
sys_inotify_init1
compat_sys_preadv
compat_sys_pwritev
compat_sys_rt_tgsigqueueinfo
sys_perf_event_open
sys_accept4
compat_sys_recvmmsg
sys_fanotify_init
sys_32_fanotify_mark
sys_prlimit64
sys_name_to_handle_at
open_by_handle_at
clock_adjtime
sys_syncfs
compat_sys_sendmmsg
sys_setns
compat_sys_process_vm_readv
compat_sys_process_vm_writev
sys_kcmp
