include(defs)c32`'dnl
sys_restart_syscall
sys_exit
sys_fork
sys_read
sys_write
open
close
sys_ni_syscall
sys_creat
link
unlink
execve
chdir
sys_ni_syscall
mknod
chmod
sys_lchown16
sys_ni_syscall
sys_ni_syscall
compat_sys_lseek_wrapper
getpid
mount
sys_ni_syscall
sys_setuid16
sys_getuid16
sys_ni_syscall
compat_sys_ptrace
sys_ni_syscall
sys_ni_syscall
sys_pause
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
access
nice
sys_ni_syscall
sys_sync
sys_kill
rename
mkdir
rmdir
sys_dup
sys_pipe
times
sys_ni_syscall
sys_brk
sys_setgid16
sys_getgid16
sys_ni_syscall
sys_geteuid16
sys_getegid16
sys_acct
umount
sys_ni_syscall
compat_sys_ioctl
compat_sys_fcntl
sys_ni_syscall
sys_setpgid
sys_ni_syscall
sys_ni_syscall
sys_umask
sys_chroot
compat_sys_ustat
sys_dup2
sys_getppid
sys_getpgrp
sys_setsid
compat_sys_sigaction
sys_ni_syscall
sys_ni_syscall
sys_setreuid16
sys_setregid16
sys_sigsuspend
compat_sys_sigpending
sys_sethostname
compat_sys_setrlimit
sys_ni_syscall
compat_sys_getrusage
compat_sys_gettimeofday
compat_sys_settimeofday
sys_getgroups16
sys_setgroups16
sys_ni_syscall
symlink
sys_ni_syscall
readlink
sys_uselib
sys_swapon
sys_reboot
sys_ni_syscall
sys_ni_syscall
sys_munmap
sys_truncate
sys_ftruncate
fchmod
sys_fchown16
sys_getpriority
sys_setpriority
sys_ni_syscall
compat_sys_statfs
compat_sys_fstatfs
sys_ni_syscall
sys_ni_syscall
sys_syslog
setitimer
getitimer
compat_sys_newstat
compat_sys_newlstat
compat_sys_newfstat
sys_ni_syscall
sys_ni_syscall
sys_vhangup
sys_ni_syscall
sys_ni_syscall
compat_sys_wait4
sys_swapoff
compat_sys_sysinfo
sys_ni_syscall
sys_fsync
compat_sys_sigreturn_wrapper
sys_clone
sys_setdomainname
sys_newuname
sys_ni_syscall
compat_sys_adjtimex
sys_mprotect
compat_sys_sigprocmask
sys_ni_syscall
sys_init_module
sys_delete_module
sys_ni_syscall
sys_quotactl
sys_getpgid
sys_fchdir
sys_bdflush
sys_sysfs
sys_personality
sys_ni_syscall
sys_setfsuid16
sys_setfsgid16
sys_llseek
compat_sys_getdents
compat_sys_select
sys_flock
sys_msync
compat_sys_readv
compat_sys_writev
sys_getsid
sys_fdatasync
compat_sys_sysctl
sys_mlock
sys_munlock
sys_mlockall
sys_munlockall
sched_setparam
sched_getparam
sched_setscheduler
sched_getscheduler
sched_yield
sched_get_priority_max
sched_get_priority_min
sched_rr_get_interval
nanosleep
sys_mremap
sys_setresuid16
sys_getresuid16
sys_ni_syscall
sys_ni_syscall
sys_poll
sys_ni_syscall
sys_setresgid16
sys_getresgid16
sys_prctl
compat_sys_rt_sigreturn_wrapper
compat_sys_rt_sigaction
sys_rt_sigprocmask
sys_rt_sigpending
compat_sys_rt_sigtimedwait
compat_sys_rt_sigqueueinfo
sys_rt_sigsuspend
compat_sys_pread64_wrapper
compat_sys_pwrite64_wrapper
sys_chown16
sys_getcwd
sys_capget
sys_capset
compat_sys_sigaltstack
compat_sys_sendfile
sys_ni_syscall
sys_ni_syscall
sys_vfork
compat_sys_getrlimit
sys_mmap_pgoff
compat_sys_truncate64_wrapper
compat_sys_ftruncate64_wrapper
sys_stat64
sys_lstat64
sys_fstat64
sys_lchown
sys_getuid
sys_getgid
sys_geteuid
sys_getegid
sys_setreuid
sys_setregid
sys_getgroups
sys_setgroups
sys_fchown
sys_setresuid
sys_getresuid
sys_setresgid
sys_getresgid
sys_chown
sys_setuid
sys_setgid
sys_setfsuid
sys_setfsgid
compat_sys_getdents64
sys_pivot_root
sys_mincore
sys_madvise
compat_sys_fcntl64
sys_ni_syscall
sys_ni_syscall
sys_gettid
compat_sys_readahead_wrapper
setxattr
lsetxattr
fsetxattr
getxattr
lgetxattr
fgetxattr
listxattr
llistxattr
flistxattr
removexattr
lremovexattr
fremovexattr
sys_tkill
sys_sendfile64
compat_sys_futex
sched_setaffinity
sched_getaffinity
io_setup
io_destroy
io_getevents
io_submit
io_cancel
sys_exit_group
compat_sys_lookup_dcookie
sys_epoll_create
sys_epoll_ctl
sys_epoll_wait
sys_remap_file_pages
sys_ni_syscall
sys_ni_syscall
sys_set_tid_address
timer_create
timer_settime
timer_gettime
timer_getoverrun
timer_delete
clock_settime
clock_gettime
clock_getres
clock_nanosleep
compat_sys_statfs64_wrapper
compat_sys_fstatfs64_wrapper
sys_tgkill
utimes
compat_sys_fadvise64_64_wrapper
sys_pciconfig_iobase
sys_pciconfig_read
sys_pciconfig_write
compat_sys_mq_open
sys_mq_unlink
compat_sys_mq_timedsend
compat_sys_mq_timedreceive
compat_sys_mq_notify
compat_sys_mq_getsetattr
compat_sys_waitid
sys_socket
sys_bind
sys_connect
sys_listen
sys_accept
sys_getsockname
sys_getpeername
sys_socketpair
sys_send
sys_sendto
compat_sys_recv
compat_sys_recvfrom
sys_shutdown
compat_sys_setsockopt
compat_sys_getsockopt
compat_sys_sendmsg
compat_sys_recvmsg
sys_semop
sys_semget
compat_sys_semctl
compat_sys_msgsnd
compat_sys_msgrcv
sys_msgget
compat_sys_msgctl
compat_sys_shmat
sys_shmdt
sys_shmget
compat_sys_shmctl
sys_add_key
sys_request_key
compat_sys_keyctl
compat_sys_semtimedop
sys_ni_syscall
sys_ioprio_set
sys_ioprio_get
sys_inotify_init
sys_inotify_add_watch
sys_inotify_rm_watch
compat_sys_mbind
compat_sys_get_mempolicy
compat_sys_set_mempolicy
openat
mkdirat
mknodat
sys_fchownat
futimesat
sys_fstatat64
unlinkat
renameat
linkat
symlinkat
readlinkat
fchmodat
faccessat
compat_sys_pselect6
compat_sys_ppoll
sys_unshare
compat_sys_set_robust_list
compat_sys_get_robust_list
sys_splice
compat_sys_sync_file_range2_wrapper
sys_tee
compat_sys_vmsplice
compat_sys_move_pages
sys_getcpu
compat_sys_epoll_pwait
compat_sys_kexec_load
utimensat
signalfd
sys_timerfd_create
sys_eventfd
compat_sys_fallocate_wrapper
compat_sys_timerfd_settime
compat_sys_timerfd_gettime
signalfd4
sys_eventfd2
sys_epoll_create1
sys_dup3
sys_pipe2
sys_inotify_init1
compat_sys_preadv
compat_sys_pwritev
compat_sys_rt_tgsigqueueinfo
sys_perf_event_open
compat_sys_recvmmsg
sys_accept4
sys_fanotify_init
compat_sys_fanotify_mark_wrapper
sys_prlimit64
sys_name_to_handle_at
open_by_handle_at
BUG(sys_clock_adjtime,	should be compat_sys_clock_adjtime)
sys_syncfs
