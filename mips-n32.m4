include(defs)n32`'define(`narrow_time',1)dnl
sys_read
sys_write
open
close
sys_newstat
sys_newfstat
sys_newlstat
sys_poll
sys_lseek
sys_mips_mmap
sys_mprotect
sys_munmap
sys_brk
sys_32_rt_sigaction
compat_sys_rt_sigprocmask
compat_sys_ioctl
sys_pread64
sys_pwrite64
compat_sys_readv
compat_sys_writev
access
sysm_pipe
compat_sys_select
sched_yield
sys_mremap
sys_msync
sys_mincore
sys_madvise
sys_shmget
sys_shmat
compat_sys_shmctl
sys_dup
sys_dup2
sys_pause
nanosleep
getitimer
setitimer
sys_alarm
getpid
sys_32_sendfile
sys_socket
sys_connect
sys_accept
sys_sendto
compat_sys_recvfrom
compat_sys_sendmsg
compat_sys_recvmsg
sys_shutdown
sys_bind
sys_listen
sys_getsockname
sys_getpeername
sys_socketpair
compat_sys_setsockopt
sys_getsockopt
sys_clone
sys_fork
execve
sys_exit
compat_sys_wait4
sys_kill
sys_newuname
sys_semget
sys_semop
sys_n32_semctl
sys_shmdt
sys_msgget
sys_n32_msgsnd
sys_n32_msgrcv
compat_sys_msgctl
compat_sys_fcntl
sys_flock
sys_fsync
sys_fdatasync
sys_truncate
sys_ftruncate
compat_sys_getdents
sys_getcwd
chdir
sys_fchdir
rename
mkdir
rmdir
sys_creat
link
unlink
symlink
readlink
chmod
fchmod
sys_chown
sys_fchown
sys_lchown
sys_umask
compat_sys_gettimeofday
compat_sys_getrlimit
compat_sys_getrusage
compat_sys_sysinfo
times
compat_sys_ptrace
sys_getuid
sys_syslog
sys_getgid
sys_setuid
sys_setgid
sys_geteuid
sys_getegid
sys_setpgid
sys_getppid
sys_getpgrp
sys_setsid
sys_setreuid
sys_setregid
sys_getgroups
sys_setgroups
sys_setresuid
sys_getresuid
sys_setresgid
sys_getresgid
sys_getpgid
sys_setfsuid
sys_setfsgid
sys_getsid
sys_capget
sys_capset
compat_sys_rt_sigpending
compat_sys_rt_sigtimedwait
compat_sys_rt_sigqueueinfo
compat_sys_rt_sigsuspend
compat_sys_sigaltstack
utime
mknod
sys_32_personality
compat_sys_ustat
compat_sys_statfs
compat_sys_fstatfs
sys_sysfs
sys_getpriority
sys_setpriority
sched_setparam
sched_getparam
sched_setscheduler
sched_getscheduler
sched_get_priority_max
sched_get_priority_min
sched_rr_get_interval
sys_mlock
sys_munlock
sys_mlockall
sys_munlockall
sys_vhangup
sys_pivot_root
compat_sys_sysctl
sys_prctl
compat_sys_adjtimex
compat_sys_setrlimit
sys_chroot
sys_sync
sys_acct
compat_sys_settimeofday
mount
umount
sys_swapon
sys_swapoff
sys_reboot
sys_sethostname
sys_setdomainname
sys_ni_syscall
sys_init_module
sys_delete_module
sys_ni_syscall
sys_ni_syscall
sys_quotactl
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
sys_gettid
sys_readahead
setxattr
lsetxattr
fsetxattr
getxattr
lgetxattr
fgetxattr
listxattr
llistxattr
flistxattr
removexattr
lremovexattr
fremovexattr
sys_tkill
sys_ni_syscall
sys_32_futex
sched_setaffinity
sched_getaffinity
sys_cacheflush
sys_cachectl
sys_sysmips
io_setup
io_destroy
io_getevents
io_submit
io_cancel
sys_exit_group
sys_lookup_dcookie
sys_epoll_create
sys_epoll_ctl
sys_epoll_wait
sys_remap_file_pages
sysn32_rt_sigreturn
compat_sys_fcntl64
sys_set_tid_address
sys_restart_syscall
compat_sys_semtimedop
sys_fadvise64_64
compat_sys_statfs64
compat_sys_fstatfs64
sys_sendfile64
timer_create
timer_settime
timer_gettime
timer_getoverrun
timer_delete
clock_settime
clock_gettime
clock_getres
clock_nanosleep
sys_tgkill
utimes
sys_ni_syscall
sys_ni_syscall
sys_ni_syscall
compat_sys_mq_open
sys_mq_unlink
compat_sys_mq_timedsend
compat_sys_mq_timedreceive
compat_sys_mq_notify
compat_sys_mq_getsetattr
sys_ni_syscall
compat_sys_waitid
sys_ni_syscall
sys_add_key
sys_request_key
sys_keyctl
sys_set_thread_area
sys_inotify_init
sys_inotify_add_watch
sys_inotify_rm_watch
sys_migrate_pages
openat
mkdirat
mknodat
sys_fchownat
futimesat
sys_newfstatat
unlinkat
renameat
linkat
symlinkat
readlinkat
fchmodat
faccessat
compat_sys_pselect6
compat_sys_ppoll
sys_unshare
sys_splice
sys_sync_file_range
sys_tee
compat_sys_vmsplice
sys_move_pages
compat_sys_set_robust_list
compat_sys_get_robust_list
compat_sys_kexec_load
sys_getcpu
compat_sys_epoll_pwait
sys_ioprio_set
sys_ioprio_get
utimensat
signalfd
sys_ni_syscall
sys_eventfd
sys_fallocate
sys_timerfd_create
compat_sys_timerfd_gettime
compat_sys_timerfd_settime
BUG(sys_signalfd4, should be compat_sys_signalfd4)
sys_eventfd2
sys_epoll_create1
sys_dup3
sys_pipe2
sys_inotify_init1
sys_preadv
sys_pwritev
compat_sys_rt_tgsigqueueinfo
sys_perf_event_open
sys_accept4
compat_sys_recvmmsg
sys_getdents64
sys_fanotify_init
sys_fanotify_mark
sys_prlimit64
sys_name_to_handle_at
open_by_handle_at
clock_adjtime
sys_syncfs
compat_sys_sendmmsg
sys_setns
compat_sys_process_vm_readv
compat_sys_process_vm_writev
sys_kcmp
